from multiprocessing import Pool, cpu_count

import cv2
import torch
import numpy as np
from pytube import YouTube

import EpipolarPose.lib.models as models
from EpipolarPose.lib.core.config import config
from EpipolarPose.lib.core.config import update_config
from EpipolarPose.lib.core.integral_loss import get_joint_location_result
from EpipolarPose.lib.utils.img_utils import convert_cvimg_to_tensor
from EpipolarPose.lib.utils.vis import drawskeleton, show3Dpose

'''
def on_progress(stream, _, __, bytes_remaining):
    global video_pbar, audio_pbar
    if stream.includes_video_track:
        video_pbar.n = (1 - (bytes_remaining / float(stream.filesize))) * 100
        video_pbar.refresh()
    else:
        audio_pbar.n = (1 - (bytes_remaining / float(stream.filesize))) * 100
        audio_pbar.refresh()


def on_complete(stream, file_handle):
    if stream.includes_video_track:
        video_pbar.close()
    else:
        audio_pbar.close()
'''
image_size = None
model = None


def setup_model(cfg_file):
    global image_size
    update_config(cfg_file)

    torch.backends.cudnn.benchmark = config.CUDNN.BENCHMARK
    torch.backends.cudnn.deterministic = config.CUDNN.DETERMINISTIC
    torch.backends.cudnn.enabled = config.CUDNN.ENABLED

    image_size = config.MODEL.IMAGE_SIZE[0]

    model = models.pose3d_resnet.get_pose_net(config, is_train=False)
    gpus = [int(i) for i in config.GPUS.split(',')]
    model = torch.nn.DataParallel(model, device_ids=gpus).cuda()
    print('Created model...')

    checkpoint = torch.load(config.MODEL.RESUME)
    model.load_state_dict(checkpoint)
    model.eval()
    print('Loaded pretrained weights...')

    return model


def preprocess_image(image):
    global image_size
    image = cv2.resize(image, (image_size, image_size))

    img_height, img_width, img_channels = image.shape
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    img_patch = convert_cvimg_to_tensor(image)

    mean = np.array([123.675, 116.280, 103.530])
    std = np.array([58.395, 57.120, 57.375])

    # apply normalization
    for n_c in range(img_channels):
        if mean is not None and std is not None:
            img_patch[n_c, :, :] = (img_patch[n_c, :, :] - mean[n_c]) / std[n_c]
    img_patch = torch.from_numpy(img_patch).cuda()

    return img_patch[None, ...]


def predict_joint_locations(img_path=None, image=None):
    global model
    if img_path is not None and image is not None:
        raise Exception("preprocess_image takes either an image or a path. Insert HALO \"Wait, that's illegal\""
                        " meme here.")
    if img_path is not None:
        image = cv2.imread(img_path, cv2.IMREAD_COLOR | cv2.IMREAD_IGNORE_ORIENTATION)
    preprocessed_image = preprocess_image(image)
    preds = model(preprocessed_image)
    preds = get_joint_location_result(image_size, image_size, preds)[0, :, :3]

    return preds


def download_audio(video_url):
    stream_object = YouTube(video_url)
    audio_stream = stream_object.streams.filter(only_audio=True).order_by('abr').last()
    return audio_stream.download('./audios/')


def download_video(video_url):
    stream_object = YouTube(video_url)
    video_stream = stream_object.streams.filter(only_video=True).order_by('resolution').last()
    return video_stream.download('./temp/')


def process_video(video_url):
    download_video(video_url)
    return 0


if __name__ == '__main__':
    # main('./Solo dances.txt')
    model = setup_model('./EpipolarPose/experiments/mpii/valid.yaml')
    '''
    print("Beginning video download...")
    video_file = download_video('https://www.youtube.com/watch?v=PJXdJAJ1OPc')
    print("Finished video download, opening vidcap...")
    '''
    video_file = './temp/Hayley Houghton - Rise Up.webm'
    cap = cv2.VideoCapture(video_file)
    if not cap.isOpened():
        print("Error opening the video file.")
    else:
        print("Opened vidcap, attempting prediction...")
    while cap.isOpened():
        ret, frame = cap.read()
        if ret:
            preds = predict_joint_locations(image=frame)
            print(preds)
        else:
            break
    cap.release()



