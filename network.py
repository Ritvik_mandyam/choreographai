import pandas as pd
from keras import Input, Model

from keras.layers import LSTM, Dense
import numpy as np


def model_fn():
    audio = Input((256))
    net, h = LSTM(256, return_sequences=True)
    net = LSTM(512)(net)
    net = LSTM(256)(net)
    preds = Dense(14)(net)

    return Model(inputs=audio, outputs=preds)


def data_generator():
    mapping = pd.read_csv('./data/mapping.csv')

    for row in mapping.itertuples(index=False):
        input_audio = row[0]
        input_poses = pd.read_csv(row[1])

        yield np.asarray(input_audio), np.asarray(input_poses)


def train():
    model = model_fn()
    model.fit_generator(data_generator(), epochs=200)