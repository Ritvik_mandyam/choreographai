import pandas as pd
from pytube import YouTube


def on_progress(stream, chunk, file_handle, bytes_remaining):
    print('{} : {}'.format(stream, bytes_remaining))


def main(videos_file):
    video_urls = pd.read_csv(videos_file)
    for label, video_url in video_urls.itertuples():
        stream_object = YouTube(video_url)
        stream_object.register_on_progress_callback(on_progress)
        audio_stream = stream_object.streams.filter(only_audio=True).order_by('abr').last()
        video_stream = stream_object.streams.filter(only_video=True).order_by('resolution').last()

        audio_stream.download('./audios/')
        video_stream.download('./temp/')


if __name__ == '__main__':
    main('./Solo dances.txt')